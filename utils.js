var Utils = {
    creteElement: function (tagName, parentElement, className) {
        var element = parentElement.ownerDocument.createElement(tagName);
        parentElement.appendChild(element);
        if (typeof(className) == 'string') {
            element.className = className;
        }
        return element;
    }
};